package com.example.demo.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// 時間の絞り込み
	public List<Report> findReportBetween(String startDate, String endDate) {
		String start;
		String end;
		DateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


			if (startDate != "") {
				start = startDate + " 00:00:00";
			} else {
				start = "2000-01-01 00:00:00";
			}

			if (endDate != "") {
				end = endDate + " 23:59:59";
			} else {
				Date date = new Date();
				end = sdFormat.format(date);
			}

			Date begDate = null;
			try {
				begDate = sdFormat.parse(start);
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			Date finDate = null;
			try {
				finDate = sdFormat.parse(end);
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			List<Report> narrow = (List<Report>) reportRepository.findByCreatedDateBetween(begDate, finDate);
			return narrow;
}
}
